package com.clave.s03activity;

import java.util.Scanner;

public class ComputeFactorial {

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        int num = 1;
        int x = 1;
        int y = 1;
        System.out.println(num);
        System.out.println(x);

        try {
            do {
                System.out.println("Find the factorial of (please enter a positive number):");
                num = in.nextInt();
            } while (num < 0);
                for (x = 1; x <= num; x++) {
                    y = y * x;
                }
            System.out.println(num + "! : " + y);
        } catch (Exception err) {
            System.out.println("Invalid input!");
        }
    }
}
